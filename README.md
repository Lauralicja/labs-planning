# Introduction to Planning

Laboratories created by Mateusz Ślażyński for Knowledge Representation and Reasoning course at AGH University of Science and Technology in Kraków.

## Setup 

* [ ] Fork this project
  * [how to create a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
* [ ] Add @mateusz.slazynski as the new project's member (role: **maintainer**)
  * [how to add an user](https://docs.gitlab.com/ee/user/project/members/index.html#add-a-user)
* [ ] Switch your fork's visibility to **private**
  * [how to change visibility](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)


## How To Submit Solutions

* [ ] Clone repository: git clone:
    ```bash 
    git clone <repository url>
    ```
* [ ] Create a new branch, i.e. 'lab3'
    ```bash 
    git checkout -b <branch name>
    ```
* [ ] Solve the exercises 
    * use MiniZincIDE, whatever
* [ ] Commit your changes
    ```bash
    git add <path to the changed files>
    git commit -m <commit message>
    ```
* [ ] Push changes to the gitlab
    ```bash
    # -u origin <branch name> is only required first time you push a new branch
    git push -u origin <branch name>
    ```
* [ ] Create a MR (Merge Request) from the new branch to the 'master' and assign it to @mateusz.slazynski. 
    * put your name in the MR description
    * [how to create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

## Lab Instructions

All instructions are available via [the course wiki](https://ai.ia.agh.edu.pl/en:dydaktyka:krr:start2020#laboratories). 
All files required to solve the assignments are already included in the repository.

## Tools 

This repository contains also a FastForward planner suitable for the modern operating systems, including:

- sources (for Windows and Unix). 

- precompiled binaries for several platforms:
  - linux-64bit
  - linux-32bit
  - macOS
    - note: the binary is dynamically linked to `libgcc_s.1.dylib`, so make sure you have one available. You can install this library via  `conda` (package `libgcc`), `homebrew` (package `gcc`) or whatever, just make sure that `$DYLD_LIBRARY_PATH` constains directory with the library. `ff-macos.sh` script may prove helpful, just edit the library path to use it ;)
  - windows-32bit (should also work on 64bit)

### How To Build Your Own FF

#### Requirements

- Windows (install via Cygwin):
  - mingw64
  - bison
  - flex
  - automake
- Unix:
  - gcc (not clang, on macOS you can use gcc provided by anaconda)
  - bison
  - flex
  - automake
  
#### Building

Change directory to the appriopriate src directory and run ```make```.
The `ff` binary should magically appear ;)
