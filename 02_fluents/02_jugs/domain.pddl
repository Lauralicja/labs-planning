(define (domain jug-pouring)
(:requirements :typing :fluents)
(:types jug)
  (:functions
		;amount of water in a jug
		(amount ?j - jug)
		;capacity of a jug
		(capacity ?j - jug)
		;how many actions were already done
		(pour-counter))
    (:action pour
        :parameters (?jug1 ?jug2 - jug)
        :precondition (> (- (capacity ?jug2) (amount ?jug2)) 0)
        :effect (and
        ; if amount jug2 > jug1, pour all jug1
            (when 
                (>= (- (capacity ?jug2) (amount ?jug2)) (amount ?jug1))
                (and 
                (increase (amount ?jug2) (amount ?jug1))(assign (amount ?jug1) 0) )
            )
        ; else pour all to jug2
      	    (when 
      	        (< (- (capacity ?jug2) (amount ?jug2)) (amount ?jug1))
                (and 
                (decrease (amount ?jug1) (- (capacity ?jug2) (amount ?jug2))) (assign (amount ?jug2) (capacity ?jug2)))
		    )
		    (increase (pour-counter) 1))
		)
	(:action fill
        :parameters (?jug -jug)
        :precondition(>(capacity ?jug) (amount ?jug))
        :effect(and
            (assign(amount ?jug) (capacity ?jug))
            (increase(pour-counter) 1)
        )
    )
    
    (:action empty
        :parameters (?jug - jug)
        :precondition(>(amount ?jug) 0)
        :effect(and
            (assign (amount ?jug) 0)
            (increase(pour-counter) 1)
        )
    )
)
 
