(define (problem zombie-escape)
	(:domain zombie-escape)
	(:objects
		; TODO: actors
		adam john alice ann -human  left right - position)
			(:init
		; TODO: initial conditions
		(= (time-count) 0)
		(= (time adam) 1)
		(= (time john) 5)
		(= (time alice) 3)
		(= (time ann) 4)
		(pos_h adam left)
		(pos_h john left)
		(pos_h alice left)
		(pos_h ann left)
		(pos_b left)
		)
	(:goal (and 
	
		(pos_h adam right)
		(pos_h john right)
		(pos_h alice right)
		(pos_h ann right)
		(pos_b right)))
		
	(:metric minimize(time-count))
)
