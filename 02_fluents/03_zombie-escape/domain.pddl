(define (domain zombie-escape)
	(:requirements :typing :fluents)
	(:types human position)
	(:functions
		; TODO: fluents:
	      ; - time required to pass the bridge by actor
		(time ?hum - human) -number
			; - count time
		(time-count) - number)
	(:predicates 
		; TODO: predicates:
		; - where is the actor
			(pos_h ?hum - human ?l - position)
		; - where is the lantern
		(pos_b ?l -position)
		)
	; crosses the bridge
	(:action cross
	:parameters(?X1 - human ?X2 -human ?L1 -position ?L2 -position)	
	:precondition(and
	             (pos_h ?X1  ?L1)
	             (pos_h ?X2  ?L1)
	             (pos_b ?L1)
	              )
	 :effect (and
	        (not(pos_h ?X1  ?L1))
	        (not(pos_h ?X2  ?L1))
	        (not(pos_b ?L1))
	        (pos_h ?X1  ?L2)
	        (pos_h ?X2  ?L2)
	        (pos_b ?L2)
	        (when (>=(time ?X1) (time ?X2)) (increase (time-count) (time ?X1)))
	        (when (>(time ?X2) (time ?X1)) (increase (time-count) (time ?X2)))
	        )
)
)
