; generalize blocks world by introducing the blocks size

(define (domain hanoi) 
  (:requirements :adl :typing :fluents) 
  (:types block place)
  (:predicates (on ?x ?y - block) 		; block ?x is on block ?y
	       (ontable ?x ?pl)		; block ?x on the table
	       (handempty)		; robot’s arm is empty
	       (holding ?x)		; robot’s arm is holding ?x
	       (bigger ?x ?y - block) ; x is bigger than y
	       (tableempty ?pl)
	       )
  (:action pick-up			; action „pick up from the table”
	     :parameters (?block - block ?place - place)
	     :precondition (and (not (exists (?x - block) (on ?x ?block))) (ontable ?block ?place) (handempty) )
	     :effect
	     (and (not (ontable ?block ?place))
		  (not (handempty))
		  (holding ?block)
	      (tableempty ?place))
    )
  ; action „put on the table”
    (:action put-on-the-table
	    :parameters (?block - block ?place)
	    :precondition (and (holding ?block) (tableempty ?place))
		:effect
		(and (ontable ?block ?place)
		     (handempty) 
			 (not (holding ?block))
			 (not(tableempty ?place))
	   )
    )
  ; action „put block A on block B” 
    (:action put-A-on-B			
	    :parameters (?blockA ?blockB - block)
	    :precondition (and (not (exists (?x - block) (on ?x ?blockB))) (holding ?blockA) (not (bigger ?blockA ?blockB)))
	    :effect
	    (and    (on ?blockA ?blockB) 
	            (handempty) 
	            (not (holding ?blockA)) 
	    )
	  )
  ; action „take block A off block B”
    (:action take-A-off-B			
	    :parameters (?blockA ?blockB - block)
        :precondition (and (on ?blockA ?blockB) (handempty) (not (exists (?x - block) (on ?x ?blockA))) )
	    :effect
	    (and (holding ?blockA) 
	        (not (handempty)) 
	        (not (on ?blockA ?blockB))  
	    )
    )
)
