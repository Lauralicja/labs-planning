; create a problem for the newly defined hanoi towers 

(define (problem easy)
  (:domain hanoi)
  (:objects a b c - block
  l r m - place)
  (:init (on a b) (ontable b l) (ontable c r) (bigger c b) (bigger b a) (handempty) (tableempty m))
  (:goal (and (on b c) (on a b)))
)
