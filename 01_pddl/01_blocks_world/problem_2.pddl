; fill based on the STRIPS representation from lab 00_intro

;%% EXAMPLE II
;problem2 :-
;  plan(
;  [on(a,c),ontable(b),ontable(c),clear(a),clear(b),handempty],
;  [on(b,c), on(a,b)]).


(define (problem two)
  (:domain blocksworld)
  (:objects a b c)
  (:init (on a c) (ontable b) (ontable c) (clear a) (clear b) (handempty))
  (:goal (and (on b c) (on a b)))
)
