(define (domain blocksworld) 
  (:requirements :adl :typing) 
  (:types block)
  (:predicates (on ?x ?y - block) 		; block ?x is on block ?y
	       (ontable ?x)		; block ?x on the table
	       (handempty)		; robot’s arm is empty
	       (holding ?x)		; robot’s arm is holding ?x
	       )
  (:action pick-up			; action „pick up from the table”
	     :parameters (?block - block)
	     :precondition (and (not (exists (?x - block) (on ?x ?block))) (ontable ?block) (handempty))
	     :effect
	     (and (not (ontable ?block))
		  (not (handempty))
		  (holding ?block))
    )
  ; action „put on the table”
    (:action put-on-the-table
	    :parameters (?block - block)
	    :precondition (holding ?block)
		:effect
		(and (ontable ?block)
		     (handempty) 
			 (not (holding ?block))
	   )
    )
  ; action „put block A on block B” 
    (:action put-A-on-B			
	    :parameters (?blockA ?blockB - block)
	    :precondition (and (not (exists (?x - block) (on ?x ?blockB))) (holding ?blockA))
	    :effect
	    (and    (on ?blockA ?blockB) 
	            (handempty) 
	            (not (holding ?blockA)) 
	    )
	  )
  ; action „take block A off block B”
    (:action take-A-off-B			
	    :parameters (?blockA ?blockB - block)
        :precondition (and (on ?blockA ?blockB) (handempty) (not (exists (?x - block) (on ?x ?blockA))) )
	    :effect
	    (and (holding ?blockA) 
	        (not (handempty)) 
	        (not (on ?blockA ?blockB))  
	    )
    )
)
