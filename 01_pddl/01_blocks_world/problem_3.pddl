; fill based on the STRIPS representation from lab 00_intro

;%% EXAMPLE III
;problem3 :-
;  plan(
;    [on(c,a),ontable(a),ontable(b),clear(b),clear(c),handempty],
;    [on(b,c), on(a,b)]).

(define (problem three)
  (:domain blocksworld)
  (:objects a b c)
  (:init (on c a) (ontable a) (ontable b) (clear b) (clear c) (handempty))
  (:goal (and (on b c) (on a b)))
)
